export interface IUser {
  userId: string;
  userName: string;
}

export interface IRoom {
  users: IUser[];
}

export type IRoomCollections = {
  [roomId: string]: IRoom;
};

export interface ISocketControllerClass {
  new (): void;
  disconnect: () => void;
  chatMessage: (msg: string) => void;
  leftRoom: (roomId: string, user: IUser) => void;
  joinRoom: (roomId: string, user: IUser) => void;
  createRoom: (roomId: string, user: IUser) => void;
  roomMessage: (msg: string, roomId: string, user: IUser) => void;
}

export interface IStateControllerClass {
  new (): void;
  getRoomMembers: (roomId: string) => IUser[];
  addRoomMember: (roomId: string, user: IUser) => void;
}
