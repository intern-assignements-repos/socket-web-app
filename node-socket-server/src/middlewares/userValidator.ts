import { IUser } from "../interface";

const userValidator = (user: IUser) => {
  const { userId, userName } = user;

  if (!userId) return false;

  if (!userName) return false;

  return true;
};

export default userValidator;
