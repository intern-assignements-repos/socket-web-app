import cors from "cors";
import express from "express";
import { Server, Socket } from "socket.io";

import StateController from "./controllers/StateController";
import SocketController from "./controllers/SocketController";

const port = process.env.PORT || 5000;
const host = process.env.HOST || "localhost";

const app = express();

app.use(cors());
app.use(express.static("public"));

app.get("/", (req, res) => res.sendFile(__dirname + "/templates/index.html"));

SocketController.io = new Server(
  app.listen(port, () =>
    console.log(`🚂🚃🚃 Server running at http://${host}:${port}`)
  ),
  {
    cors: {
      origin: "*",
      methods: ["GET", "POST"],
    },
  }
);

SocketController.io.on("connection", (socket: Socket) => {
  SocketController.socket = socket;
  StateController.printRoomCollection();
  socket.on("join-room", SocketController.joinRoom);
  socket.on("left-room", SocketController.leftRoom);
  socket.on("disconnect", SocketController.disconnect);
  socket.on("create-room", SocketController.createRoom);
  socket.on("chat-message", SocketController.chatMessage);
  socket.on("room-message", SocketController.roomMessage);
});
