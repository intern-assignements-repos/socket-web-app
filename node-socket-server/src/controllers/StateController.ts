import roomCollection from "../states";

import { IRoom, IUser, IStateControllerClass } from "../interface";
import userValidator from "../middlewares/userValidator";

const staticImplements =
  <T>() =>
  <U extends T>(constructor: U) =>
    constructor;

@staticImplements<IStateControllerClass>()
export default class StateController {  

  static getRoomMembers(roomId: string) {
    try {
      if (!roomCollection[roomId]) throw new Error("Room does not Exists");

      const { users } = <{ users: IUser[] }>roomCollection[roomId];

      return users;
    } catch (err: Error | any) {
      console.error(err);
      return err;
    }
  }

  static addRoomMember(roomId: string, user: IUser) {
    try {
      if (!roomCollection[roomId]) throw new Error("Room does not Exists!");

      if (!userValidator(user)) throw new Error("User is Invalid!");

      const room: IRoom = roomCollection[roomId];

      const found = room.users.find(({ userId }) => userId == user.userId);

      if (found) return;
      
      room.users.push(user);

      roomCollection[roomId] = room;

      return;
    } catch (err: Error | any) {
      console.error(err);
      return err;
    }
  }

  static removeRoomMember(roomId: string, user: IUser) {
    try {
      if (!roomCollection[roomId]) throw new Error("Room does not Exists!");

      if (!userValidator(user)) throw new Error("User is Invalid!");

      const room: IRoom = roomCollection[roomId];

      const found = room.users.find(({ userId }) => userId == user.userId);

      if (!found) return;

      room.users.splice(room.users.indexOf(user), 1);

      roomCollection[roomId] = room;

      return;
    } catch (err: Error | any) {
      console.error(err);
      return err;
    }
  }

  static createRoom(roomId: string, user: IUser) {
    try {
      if (roomCollection[roomId]) throw new Error("Room already Exists!");

      const room: IRoom = { users: [user] };

      roomCollection[roomId] = room;

      return;
    } catch (err: Error | any) {
      console.error(err);
      return err;
    }
  }

  static destroyRoom(roomId: string) {
    try {
      if (!roomCollection[roomId]) throw new Error("Room does not Exists!");

      delete roomCollection[roomId];

      return;
    } catch (err: Error | any) {
      console.error(err);
      return err;
    }
  }

  static printRoomCollection() {
    Object.keys(roomCollection).forEach((k, i) => {
      console.log(
        "\nRoomId :",
        k,
        " => Users :",
        roomCollection[k].users.map((user) => user)
      );
    });
  }
}
