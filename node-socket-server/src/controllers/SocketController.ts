import { Server, Socket } from "socket.io";

import StateController from "./StateController";
import { ISocketControllerClass, IUser } from "../interface";

const staticImplements =
  <T>() =>
  <U extends T>(constructor: U) =>
    constructor;

@staticImplements<ISocketControllerClass>()
export default class SocketController {
  static io: Server;
  static socket: Socket;

  static chatMessage(msg: string) {
    SocketController.io.emit("chat-message", msg);
  }

  static roomMessage(msg: string, roomId: string, user: IUser) {    
    const message = { user, msg };
    SocketController.io.to(roomId).emit("message-received", message);
  }

  static disconnect() {
    console.log(`User disconnected: ${SocketController.socket.id}`);
  }

  static getUsers(roomId: string) {
    const members: IUser[] = StateController.getRoomMembers(roomId);

    SocketController.io.to(roomId).emit("members-received", members);
  }

  static createRoom(roomId: string, user: IUser) {
    SocketController.socket.join(roomId);
    SocketController.socket.to(roomId).emit("user-connect", user);

    StateController.createRoom(roomId, user);
    StateController.printRoomCollection();
  }

  static joinRoom(roomId: string, user: IUser) {
    SocketController.socket.join(roomId);
    SocketController.socket.to(roomId).emit("user-connect", user);

    StateController.addRoomMember(roomId, user);
    StateController.printRoomCollection();
  }

  static leftRoom(roomId: string, user: IUser) {
    SocketController.socket.to(roomId).emit("user-disconnect", user);

    StateController.removeRoomMember(roomId, user);
    StateController.printRoomCollection();
  }
}
