import LoginHeaderSection from "views/login/header.section";
import LoginMainSection from "views/login/main.section";
import "styles/login.styles.css";

const LoginPage = () => {
  return (
    <div className="container">
      <LoginHeaderSection />
      <LoginMainSection />
    </div>
  );
};

export default LoginPage;
