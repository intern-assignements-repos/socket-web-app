import React, { useEffect, useState } from "react";
// import { useNavigate, useParams } from "react-router-dom";
import { toast, ToastContainer } from "react-toastify";

import socket from "utils/socket.utils";
import notify from "utils/notify.utils";
import { getData } from "utils/login.utils";
import { useSocketConnection } from "store";
import ChatModal from "components/Modal/chat.modal";
import RoomBodySection from "views/room/body.section";

import "styles/room.styles.css";
import "react-toastify/dist/ReactToastify.css";

const RoomPage = () => {
  const isConnected = useSocketConnection((state: any) => state.isConnected);
  const setIsConnected = useSocketConnection(
    (state: any) => state.setIsConnected
  );

  const { userName, roomId, userId } = getData();

  useEffect(() => {
    const onConnect = () => setIsConnected(true);
    const onDisconnect = () => setIsConnected(false);

    socket.emit("join-room", roomId, { userId, userName });

    socket.on("connection", onConnect);
    socket.on("disconnect", onDisconnect);
    socket.on("user-connect", (data: { userName: string; userId: string }) => {
      notify(data.userName, true);
    });
    socket.on(
      "user-disconnect",
      (data: { userName: string; userId: string }) => {
        notify(data.userName, false);
      }
    );

    return () => {
      socket.off("connection", onConnect);
      socket.off("disconnect", onDisconnect);
      socket.off("user-connect", () => {
        console.log("");
      });
      socket.off("user-disconnect", () => {
        console.log("");
      });
    };
  }, []);

  return (
    <>
      <ToastContainer />
      <ChatModal />
      <RoomBodySection />
    </>
  );
};

export default RoomPage;
