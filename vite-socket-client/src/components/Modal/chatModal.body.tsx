import { IMessage } from "interfaces";
import React from "react";

interface IChatModalBodyProps {
  messageList: IMessage[];
}

const ChatModalBody = (props: IChatModalBodyProps) => {
  const { messageList } = props;
  console.log("Messages :", messageList);
  return (
    <div className="chat-body">
      {messageList?.map((message, index) => (
        <div key={index}>
          <h4>{message.sender.userName}</h4>
          <p>{message.body}</p>
        </div>
      ))}
    </div>
  );
};

export default ChatModalBody;
