import React from "react";
import { AiOutlineSend } from "react-icons/ai";
import { IChatModalFooterProps } from "interfaces";

const ChatModalFooter = (props: IChatModalFooterProps) => {
  const { sendMessage, setMessage, setIconColor, iconColor, message } = props;
  return (
    <div className="chat-modal_footer">
      <form className="message-sender" onSubmit={sendMessage}>
        <input
          type="text"
          value={message}
          onFocus={() => setIconColor(true)}
          onBlur={() => setIconColor(false)}
          placeholder="Send messages to everyone"
          onChange={(e) => setMessage(e.target.value)}
          required
        />
        <button
          className="send-icon"
          style={{
            color: `${iconColor ? `rgb(26,115,232)` : `rgba(0, 0, 0, 0.2)`}`,
          }}
          type="submit"          
        >
          <AiOutlineSend size={25} />
        </button>
      </form>
    </div>
  );
};

export default ChatModalFooter;
