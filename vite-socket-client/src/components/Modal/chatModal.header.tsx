import { GrClose } from "react-icons/gr";
import { IChatModalHeaderProps } from "interfaces";

const ChatModalHeader = ({ changeModalState }: IChatModalHeaderProps) => {  

  return (
    <div className="chat-modal_header">
      <div>
        <div className="chat-modal_header-text">In-call Messages</div>
        <div
          className="chat-modal_header-close"
          onClick={() => changeModalState()}
        >
          <GrClose size={20} />
        </div>
      </div>
      <div className="chat-modal_header-info">
        Messages can only be seen by people in the call and are deleted when the
        call ends.
      </div>
    </div>
  );
};

export default ChatModalHeader;
