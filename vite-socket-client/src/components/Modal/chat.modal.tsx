import { useEffect, useState } from "react";
import { createPortal } from "react-dom";

import { useChatModal, useMessageStore } from "store";
import ChatModalBody from "./chatModal.body";
import socket from "utils/socket.utils";
import ChatModalHeader from "./chatModal.header";

import "styles/modal.styles.css";
import ChatModalFooter from "./chatModal.footer";
import { IMessage, IUser } from "interfaces";
import { getData } from "utils/login.utils";

const Portal = ({ children }: { children: JSX.Element }) => {
  return createPortal(
    children,
    document.getElementById("chat-modal") as HTMLElement
  );
};

const ChatModal = () => {
  const [message, setMessage] = useState("");
  const [iconColor, setIconColor] = useState(false);
  
  const { userId, userName, roomId } = getData();

  const isOpen = useChatModal((state: any) => state.isOpen);
  const setMessages = useMessageStore((state: any) => state.setMessages);
  const messages = useMessageStore((state: any) => state.messages);
  const changeModalState = useChatModal((state: any) => state.changeModalState);

  const sendMessage = (e: React.FormEvent) => {
    e.preventDefault()
    const user = {
      userId,
      userName,
    };
    
    if (message) socket.emit("room-message", message, roomId, user);    
    setMessage("");    
  };

  useEffect(() => {
    socket.on("message-received", (data: { user: IUser; msg: string }) => {      
      setMessages(data.msg, data.user);
    });

    return () => {
      socket.off("message-received");
    };
  }, []);

  return (
    <Portal>
      <div
        className="chat-modal"
        style={{
          transform: `${isOpen ? `translate(0,0)` : `translate(400px, 0)`}`,
        }}
      >
        <ChatModalHeader changeModalState={changeModalState} />
        <ChatModalBody messageList={messages} />
        <ChatModalFooter
          message={message}
          iconColor={iconColor}
          setMessage={setMessage}
          sendMessage={sendMessage}
          setIconColor={setIconColor}
        />
      </div>
    </Portal>
  );
};

export default ChatModal;
