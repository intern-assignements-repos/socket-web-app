import React, { useEffect, useState } from "react";
import Webcam from "react-webcam";
import { useButtonStore } from "store";
import { getData } from "utils/login.utils";
import generateColor from "utils/colorGenerator.utils";

const VideoCard = () => {
  const videoFeed = useButtonStore((state: any) => state.video);

  const { roomId, userName } = getData();
  const [themeColor, setThemeColor] = useState("");

  useEffect(() => {
    setThemeColor(generateColor().toString());    
  }, [videoFeed]);

  return (
    <div className="video-item">
      {videoFeed ? (
        <Webcam
          mirrored
          videoConstraints={{
            height: 420,
          }}
          className="video-item"
        />
      ) : (
        <div
          className="video-img"
          style={{
            backgroundColor: themeColor,
          }}
        >
          <h1>{userName?.toString()[0].toUpperCase()}</h1>
        </div>
      )}
    </div>
  );
};

export default VideoCard;
