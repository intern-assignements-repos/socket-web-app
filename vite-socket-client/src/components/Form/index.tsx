import short from "short-uuid";
import { useState } from "react";
import { v4 as uuid } from "uuid";
import { useNavigate } from "react-router-dom";

import socket from "utils/socket.utils";
import { setLogin } from "utils/login.utils";
import { IUser } from "interfaces";

const FormComponent = () => {
  const navigate = useNavigate();
  const [name, setName] = useState("");
  const [roomId, setRoomId] = useState("");

  const handleForm = () => {
    let rId;
    const userId = short.generate();
    if (roomId) {
      rId = roomId;
      const user: IUser = {
        userId: userId,
        userName: name,
      };
      socket.emit("join-room", rId, user);
    } else {
      rId = uuid();
      const user: IUser = {
        userId: userId,
        userName: name,
      };
      socket.emit("create-room", rId, user);
    }
    setLogin(rId, name, userId);
    navigate(`/room/${rId}`);
  };

  return (
    <form onSubmit={() => handleForm()}>
      <input
        type="text"
        placeholder="Enter your name"
        required
        onChange={(e) => setName(e.target?.value)}
      />
      <input
        type="text"
        placeholder="Enter room id to join"
        onChange={(e) => setRoomId(e.target?.value.toString().trim())}
      />
      <span
        style={{
          margin: "10px",
          fontWeight: 400,
          fontSize: 14,
        }}
      >
        Leave to create a Room
      </span>
      <button type="submit">Join the Room</button>
    </form>
  );
};

export default FormComponent;
