import { useEffect } from "react";
import { BrowserRouter } from "react-router-dom";

import RouteComponent from "routes";

const App = () => {

  return (
    <BrowserRouter>
      <RouteComponent />
    </BrowserRouter>
  );
};

export default App;
