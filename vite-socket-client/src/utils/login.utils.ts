export const isLogin = (): boolean => {
    try{
        const userName = localStorage.getItem('roomUserId');
        if(userName) return true;

        return false
    }catch(err){
        throw new Error('Storage Error occured!');
    }
}

export const setLogin = (id: string, userName: string, userId: string) => {
    try{
        localStorage.setItem('roomId', id);
        localStorage.setItem('roomUserId', userId);
        localStorage.setItem('roomUserName', userName);
    } catch(err) {
        throw new Error('Storage Error occured!');
    }
}

export const getData = () => {
    try {
        const roomId = localStorage.getItem('roomId');
        const userId = localStorage.getItem('roomUserId');
        const userName = localStorage.getItem('roomUserName');

        return {
            roomId,
            userId,
            userName,
        };
    } catch(err) {
        throw new Error('Storage Error occured!');
    }
}

export const removeUserName = () => {
    try{
        localStorage.removeItem('roomId');
        localStorage.removeItem('roomUserId');
        localStorage.removeItem('roomUserName');
    }catch(err) {
        throw new Error('Storage Error occured!');
    }
}