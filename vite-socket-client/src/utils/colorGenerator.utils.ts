function generateColor() {
  const hue = Math.floor(Math.random() * 360);
  const saturation = Math.floor(Math.random() * 50) + 50;

  const lightness = 45;
  
  return `hsl(${hue}, ${saturation}%, ${lightness}%)`;
}

export default generateColor;
