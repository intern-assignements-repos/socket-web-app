import { toast } from "react-toastify";

const notify = (userName: string, joined: boolean) => {
  return joined
    ? toast(`🤝 ${userName} has joined the Room`, {
        position: "top-center",
        autoClose: 4000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "dark",
      })
    : toast(`🏃‍♀️ ${userName} has left the Room`, {
        position: "top-center",
        autoClose: 4000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "dark",
      });
};

export default notify;
