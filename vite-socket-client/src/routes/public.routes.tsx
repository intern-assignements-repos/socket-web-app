import { Navigate } from "react-router-dom";

import {getData, isLogin} from "utils/login.utils";
import { IRoutesProps } from "interfaces";

export const PublicRoutes = ({ component }: IRoutesProps) => isLogin() ? <Navigate to={`/room/${getData().roomId}`} /> : component
