import { Navigate } from "react-router-dom";

import {isLogin} from "utils/login.utils";
import { IRoutesProps } from "interfaces";

export const PrivateRoutes = ({ component }: IRoutesProps) => !isLogin() ? <Navigate to="/" /> : component