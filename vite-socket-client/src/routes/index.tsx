import { Route, Routes as ReactRoutes } from "react-router-dom";

import { PrivateRoutes } from "./private.routes";
import { PublicRoutes } from "./public.routes";
import { IRoute } from "interfaces";
import LoginPage from "pages/login";
import RoomPage from "pages/room";

export const routes: IRoute[] = [
  {
    component: <LoginPage />,
    path: "/",
    restricted: false,
  },
  {
    component: <RoomPage />,
    path: "/room/:id",
    restricted: true,
  },
];

const RouteComponent = () => {
  return (
    <ReactRoutes>
    {routes.map((route: IRoute, index: number) => {
      const { component, path, restricted } = route;

      return (
        <Route
          key={index}
          path={path}
          element={
            restricted ? (
              <PrivateRoutes component={component} />
            ) : (
              <PublicRoutes component={component} />
            )
          }
        />
      );
    })}
  </ReactRoutes>
  )
}

export default RouteComponent
