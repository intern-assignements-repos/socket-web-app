import React from "react";
import { BiVideoPlus } from "react-icons/bi";

const LoginHeaderSection = () => (
  <div className="header">
    <div className="logo">
      <BiVideoPlus size={50} />
      <span>
        <strong>Milon</strong>
        Room
      </span>
    </div>
  </div>
);

export default LoginHeaderSection;
