import { useState } from "react";
import Webcam from "react-webcam";
import {
  BsFillMicFill,
  BsFillMicMuteFill,
  BsThreeDotsVertical,
  BsFillCameraVideoFill,
  BsFillCameraVideoOffFill,
} from "react-icons/bs";
import FormComponent from "components/Form";
import { useButtonStore } from "store";
import { IButtonState } from "interfaces";

const LoginMainSection = () => {
  const videoFeed = useButtonStore((state: any) => state.video)
  const audioFeed = useButtonStore((state: any) => state.audio)

  const setVideoFeed = useButtonStore((state: any) => state.changeVideo)
  const setAudioFeed = useButtonStore((state: any) => state.changeAudio)

  // const [videoFeed, setVideoFeed] = useState(false);
  // const [audioFeed, setAudioFeed] = useState(false);

  return (
    <main>
      <div className="left-section">
        <div className="video-container">
          {
            videoFeed ?
            <Webcam
              muted
              audio={audioFeed}
              mirrored
              videoConstraints={{
                facingMode: "user",
                height: 420
              }}
            /> : <video />
          }        
          <div className="btn menu">
            <BsThreeDotsVertical size={25} />
          </div>
          <div
            className={`mic btn ${!audioFeed && `mute`}`}
            onClick={() => setAudioFeed()}
          >
            {audioFeed ? (
              <BsFillMicFill size={25} />
            ) : (
              <BsFillMicMuteFill size={25} />
            )}
          </div>
          <div
            className={`camera btn ${!videoFeed && `mute`}`}
            onClick={() => setVideoFeed()}
          >
            {videoFeed ? (
              <BsFillCameraVideoFill size={25} />
            ) : (
              <BsFillCameraVideoOffFill size={25} />
            )}
          </div>
        </div>
      </div>
      <div className="right-section">
        <h1>Ready to join?</h1>
        <FormComponent />
      </div>
    </main>
  );
};

export default LoginMainSection;
