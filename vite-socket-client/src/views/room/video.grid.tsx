import VideoCard from "components/Card/video.card";

const VideoGrid = () => {
  return (
    <div className="video-grid">
      <VideoCard />
    </div>
  );
};

export default VideoGrid;
