import React from "react";

import { useButtonStore } from "store";
import socket from "utils/socket.utils";
import { IMidFooterProps } from "interfaces";
import { removeUserName } from "utils/login.utils";

import {
  BsFillMicFill,
  BsFillMicMuteFill,
  BsThreeDotsVertical,
  BsFillCameraVideoFill,
  BsFillCameraVideoOffFill,
} from "react-icons/bs";
import { ImPhoneHangUp } from "react-icons/im";
import { IoHandRightOutline } from "react-icons/io5";
import { FaRegClosedCaptioning } from "react-icons/fa";
import { MdTagFaces, MdPresentToAll } from "react-icons/md";

const MidFooter = ({ iconSize, roomId, userName }: IMidFooterProps) => {
  const videoFeed = useButtonStore((state: any) => state.video);
  const audioFeed = useButtonStore((state: any) => state.audio);

  const setVideoFeed = useButtonStore((state: any) => state.changeVideo);
  const setAudioFeed = useButtonStore((state: any) => state.changeAudio);

  const endCall = () => {
    removeUserName();
    socket.emit("left-room", roomId, userName);

    window.location.reload();
  };

  return (
    <div className="mid section">
      <div
        className={`room-btn ${!audioFeed && "mute"}`}
        onClick={() => setAudioFeed()}
      >
        {audioFeed ? (
          <BsFillMicFill size={iconSize} />
        ) : (
          <BsFillMicMuteFill size={iconSize} />
        )}
      </div>
      <div
        className={`room-btn ${!videoFeed && "mute"}`}
        onClick={() => setVideoFeed()}
      >
        {videoFeed ? (
          <BsFillCameraVideoFill size={iconSize} />
        ) : (
          <BsFillCameraVideoOffFill size={iconSize} />
        )}
      </div>
      <div className="room-btn">
        <FaRegClosedCaptioning size={iconSize} />
      </div>
      <div className="room-btn">
        <IoHandRightOutline size={iconSize} />
      </div>
      <div className="room-btn">
        <MdTagFaces size={iconSize} />
      </div>
      <div className="room-btn">
        <MdPresentToAll size={iconSize} />
      </div>
      <div className="room-btn">
        <BsThreeDotsVertical size={iconSize} />
      </div>
      <div className="room-btn end" onClick={() => endCall()}>
        <ImPhoneHangUp size={iconSize} />
      </div>
    </div>
  );
};

export default MidFooter;
