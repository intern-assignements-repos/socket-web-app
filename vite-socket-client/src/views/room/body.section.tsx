import VideoGrid from "./video.grid";
import { getData } from "utils/login.utils";

import MidFooter from "./midFooter.section";
import LeftFooter from "./leftFooter.section";
import RightFooter from "./rightFooter.section";
import { useEffect } from "react";

const iconSize: number = 20;

const RoomBodySection = () => {
  const { roomId, userName } = getData() as {
    roomId: string;
    userName: string;
  };

  return (
    <div className="room-container">
      <VideoGrid />
      <div className="footer-section">
        <LeftFooter roomId={roomId?.toString()} />
        <MidFooter
          iconSize={iconSize}
          roomId={roomId?.toString()}
          userName={userName?.toString()}
        />
        <RightFooter iconSize={iconSize} />
      </div>
    </div>
  );
};

export default RoomBodySection;
