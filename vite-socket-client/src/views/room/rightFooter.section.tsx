import React from "react";

import { useChatModal } from "store";

import { BsChatLeftText } from "react-icons/bs";
import { IoShapesOutline } from "react-icons/io5";
import { AiOutlineInfoCircle } from "react-icons/ai";
import { MdOutlineLockPerson, MdOutlinePeopleAlt } from "react-icons/md";

const RightFooter = ({ iconSize }: { iconSize: number }) => {
  const changeModalState = useChatModal((state: any) => state.changeModalState);
  const isOpen = useChatModal((state: any) => state.isOpen);
  

  return (
    <div className="right section">
      <div className="right-btn">
        <AiOutlineInfoCircle size={iconSize} />
      </div>
      <div className="right-btn">
        <MdOutlinePeopleAlt size={iconSize} />
      </div>
      <div className="right-btn" 
      style={{
        color: `${isOpen ? `rgb(26,115,232)`:`white`}`
      }}
      onClick={() => changeModalState()}>
        <BsChatLeftText size={iconSize} />
      </div>
      <div className="right-btn">
        <IoShapesOutline size={iconSize} />
      </div>
      <div className="right-btn">
        <MdOutlineLockPerson size={iconSize} />
      </div>
    </div>
  );
};

export default RightFooter;
