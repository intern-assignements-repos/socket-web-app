import React from "react";
import { toast } from "react-toastify";

const LeftFooter = (props: { roomId: string }) => {
  const { roomId } = props;

  const copyToClipboard = () => {
    navigator.clipboard.writeText(roomId);
    toast.success("Copied to clipboard", {
      position: "top-center",
      autoClose: 2000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false,
      draggable: true,
      progress: undefined,
    });
  };

  return (
    <div className="left section">
      <span>{new Date().toTimeString().slice(0, 5)}</span> |{" "}
      <span className="copy" onClick={() => copyToClipboard()}>
        {roomId?.toString()}
      </span>{" "}
    </div>
  );
};

export default LeftFooter;
