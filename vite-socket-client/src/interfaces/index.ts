export interface IRoute {
  path: string;
  restricted: boolean;
  component: JSX.Element;
}

export interface IRoutesProps {
  component: JSX.Element;
}

export interface IButtonState {
  audio: boolean;
  video: boolean;
}

export interface IMidFooterProps {
  roomId: string;
  iconSize: number;
  userName: string;
}

export interface IUser {
  userId: string;
  userName: string;
}

export interface IChatModalHeaderProps {
  changeModalState: () => void;
}

export interface IChatModalFooterProps {
  message: string;
  iconColor: boolean;
  sendMessage: (e: React.FormEvent) => void;
  setMessage: React.Dispatch<React.SetStateAction<string>>;
  setIconColor: React.Dispatch<React.SetStateAction<boolean>>;
}

export interface IMessage {
  body: string;
  sender: IUser;
}
