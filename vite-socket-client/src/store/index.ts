import { create } from "zustand";
import { IButtonState, IMessage, IUser } from "interfaces";

export const useButtonStore = create((set) => ({
  audio: false,
  video: false,
  changeAudio: () =>
    set((state: IButtonState) => ({
      audio: !state.audio,
    })),
  changeVideo: () =>
    set((state: IButtonState) => ({
      video: !state.video,
    })),
}));

export const useChatModal = create((set) => ({
  isOpen: false,
  changeModalState: () =>
    set((state: { isOpen: boolean }) => ({
      isOpen: !state.isOpen,
    })),
}));

export const useSocketConnection = create((set) => ({
  isConnected: false,
  setIsConnected: (connected: boolean) =>
    set(() => ({
      isConnected: connected,
    })),
}));

export const useMessageStore = create((set) => ({
  messages: [],
  setMessages: (msg: string, user: IUser) =>
    set((state: { messages: IMessage[] }) => ({
      messages: [
        ...state.messages,
        {
          body: msg,
          sender: user,
        },
      ],
    })),
}));

export const useMemberStore = create((set) => ({
  members: [{}],
  setMembers: (users: IUser[]) =>
    set(() => ({
      members: users,
    })),
  addMember: (member: IUser) =>
    set((state: { members: IUser[] }) => ({
      members: [...state.members, member],
    })),
}));
